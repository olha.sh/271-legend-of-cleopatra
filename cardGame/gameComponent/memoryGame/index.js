import React from 'react';
import {StyleSheet, ImageBackground, View} from 'react-native'
import Card from '../card'
function MemoryGame({cards,handleCard}){
    return(
    <ImageBackground 
        source={require('../../assets/container.png')} 
        style={styles.fonContainer}
        resizeMode='cover'
    >
        <View style={styles.wrapperCard}>
        {cards.map((item)=> {
            return  (
        <Card 
            src={item.src} 
            key={item.id}
            openCard={item.isOpen}
            idKey={item.id}
            chooseCard={handleCard}
        />
        )
    })} 
        </View> 
    </ImageBackground>
    )
}

const styles = StyleSheet.create({

    
    fonContainer: {
        width: 400,
        height: 280,
        position: 'relative',
        justifyContent: 'center',
        alignItems: 'center'
      },
    wrapperCard: {
        margin: 10,
        position: 'absolute',
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'space-between'
    }
})


export default MemoryGame
