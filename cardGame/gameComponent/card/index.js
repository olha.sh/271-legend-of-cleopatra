import React, { useEffect} from 'react';
import {StyleSheet, View, Image, TouchableOpacity} from 'react-native'


function Card({src, openCard, idKey, chooseCard, click}){
 
   function handlChooseCard(){

          chooseCard(idKey) 

    
   }
  
    return(
        <View
            style={styles.card} 
            key={idKey} 
        >
            {openCard ? (
                <TouchableOpacity    onPress={(idKey)=>handlChooseCard(idKey)}  key={idKey} >
                    <Image source={src} 
                    style={styles.fontImg}
                    resizeMode='contain'
                    /> 
                </TouchableOpacity>
                    ) : 
                (
                    <TouchableOpacity onPress={()=>chooseCard(idKey)} >
                        <Image  source={require('../../assets/back.png')} 
                         resizeMode='contain'
                        style={ openCard ? styles.flipBack : styles.backImg}
                    /> 
                    </TouchableOpacity>
                )}
        </View>   
        )
}
const styles = StyleSheet.create({
    card:{
        width: 110,
        height: 80,
        marginBottom: 6,
    },
    flipBack :{
        display: 'none'
    },
    fontImg: {
        width: 110,
        height: 80,
    },
    backImg: {
        width: 110,
        height: 80,
      }
})




export default Card