import React  from 'react';
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Modal,
  TouchableOpacity,
  Image
} from 'react-native';

function ModalGameOver  ({isVisible, handleCloseModal, scoreValue, bestScoreVal}){
    return(
        <Modal
        transparent={true}
        visible={isVisible}
       >
            <TouchableOpacity 
              style={styles.centeredView}>
              <ImageBackground
                style={styles.modalView}
                source={require('../../assets/container.png')}
                resizeMode='cover'
                >
                <Image source={require('../../assets/score.png')}
                    style={styles.modalImg}
                    resizeMode='contain'/>
                <Text style={ styles.modalText1}>
                  Score {scoreValue}
                </Text>
                <Text style={ styles.modalText2}>
                    Best Score {bestScoreVal}
                </Text>
                <TouchableOpacity onPress={handleCloseModal}>
                    <ImageBackground 
                        source={require('../../assets/btn.png')}
                        style={styles.btn}
                        resizeMode='contain'
                    >

                    </ImageBackground>
                </TouchableOpacity>
              </ImageBackground>
              
            </TouchableOpacity>
       </Modal>


    )
}


const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: 'rgba(61, 42, 105, 0.5)'
    },
    modalView: {
        alignItems: "center",
        shadowColor: "#000",
        width: 400,
        height: 300,
        justifyContent: 'center',
        marginTop: -55

    },
    modalImg: {
        width: 250,
        height: 70
    },
    modalText1: {
        fontSize: 25,
        color: "#ffff",
        textTransform: 'uppercase',
        fontWeight: "bold",
        marginBottom: 10
      },
      modalText2: {
        fontSize: 20,
        color: "#ffff",
        textTransform: 'uppercase',
        fontWeight: "bold",
        marginBottom: 10
      },
      btn: {
          width: 150,
          height: 50
      }
})


export{
    ModalGameOver 
}