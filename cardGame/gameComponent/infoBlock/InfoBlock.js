import React from 'react'
import { StyleSheet, ImageBackground, View, Text, Image } from "react-native";

function InfoBlock (){
    return(
        <View style={styles.block}>
            <View style={styles.infoBlockContainer}>
            <Text style={styles.infoBlockTxt}>info</Text>
            <ImageBackground 
                source={require('../../assets/infoBlock.png')}
                resizeMode='cover'
                style={styles.infoBlock}
            >
                <View style={styles.blockItem}>
                    <Image style={styles.blockItemImg} source={require('../../assets/card6.png')}/>
                    <Text style={styles.blockItemTxt}>1000</Text>
                </View>
                <View style={styles.blockItem}>
                    <Image style={styles.blockItemImg} source={require('../../assets/card7.png')}/>
                    <Text style={styles.blockItemTxt}>500</Text>
                </View>
                <View style={styles.blockItem}>
                    <Image style={styles.blockItemImg} source={require('../../assets/card8.png')}/>
                    <Text style={styles.blockItemTxt}>100</Text>
                </View>

            </ImageBackground>
            </View>
        </View>
    
       
    )
}

const styles = StyleSheet.create({
    block: {
       position: 'absolute',
       bottom: 30,
       right: 10
    },
    infoBlockContainer:{
        alignItems: 'center',
        marginRight: 30,
       
    },
    infoBlockTxt: {
        color: '#91171d',
        textTransform: 'uppercase',
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 0
    },
    infoBlock: {
        width: 140,
        height: 100,
        justifyContent: 'space-around',
        padding: 5
    },
    blockItem:{
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        paddingLeft: 10

    },
    blockItemImg: {
        width: 45,
        height: 25,
        marginRight: 10
    },
    blockItemTxt: {
        color: '#fff',
        textTransform: 'uppercase',
        fontWeight: 'bold',
        fontSize: 18
    }
})


export{
    InfoBlock
}