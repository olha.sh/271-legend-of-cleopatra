import React, { useEffect} from 'react';
import {StyleSheet, View, Image, Text} from 'react-native'


function Header({scoreValue}){
   
    return(
        <View style={styles.headerContainer}>
            <Image 
                source={require('../../assets/score.png')}
                style={styles.headerImg}
                resizeMode='contain'
            />
            <View style={styles.goldContainer}>
                <Image 
                    source={require('../../assets/gold.png')}
                    style={styles.goldImg}
                />
                <Text style={styles.headerTxt}>
                    {scoreValue}
                </Text>
            </View>
        </View>   
        ) 
}

const styles = StyleSheet.create({
    headerContainer: {
        alignItems: 'center',
        height: 150
    },
    headerImg: {
        width: 300,
        // height: 100
    },
    goldContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        top: -20,
        marginBottom: 100

    },
    goldImg: {
        height: 30,
        width: 30,
        marginRight: 10,
        position: 'absolute',
        left: -35
    },
    headerTxt: {
        color: '#fff',
        fontSize: 30
    }
})

export {
    Header
}