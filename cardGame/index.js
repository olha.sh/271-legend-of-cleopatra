
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
} from 'react-native';
import {PlayScreen} from './screens/PlayScreen/PlayScreen'
import {StartScreen} from './screens/StartScreen/StartScreen'


function CardGame() {
 const [isPlay, setIsPlay] = useState(false)

  return(
   <>
      {isPlay? 
            (<PlayScreen/>)
            :
           ( <StartScreen 
                handleStart={()=>setIsPlay(true)}
                
            />)
        } 
   </>
    
  )
}

export {
   CardGame
} 