import React from 'react'
import {
    ImageBackground,
    TouchableOpacity, 
    StyleSheet
} from 'react-native'

function StartScreen({handleStart}) {
    return(
        <ImageBackground 
            source={require('../../assets/startBg.png')}
            style={styles.startBg}
        >
            <TouchableOpacity 
                onPress={handleStart} 
                style={styles.playBtn}
            >
                <ImageBackground 
                    source={require('../../assets/playBtn.png')} 
                    style={styles.imgBtn}  
                    resizeMode='contain'
                    
                >
                </ImageBackground>
            </TouchableOpacity>

        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    startBg: {
        flex: 1,
        width: '100%',
        height: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgBtn: {
        width: 250,
        height: 100,
    },
   
})

export {
    StartScreen
}