
import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  ImageBackground,
} from 'react-native';
import MemoryGame from '../../gameComponent/memoryGame'
import {Header} from '../../gameComponent/header/Header'
import {ModalGameOver } from '../../gameComponent/Modal/Modal'
import {InfoBlock} from '../../gameComponent/infoBlock/InfoBlock'



  const cards= [
    {
      src: require('../../assets/card1.png'),
      id: 0,
      isOpen: false,
      value: 0
      }, 
    {
      src: require('../../assets/card2.png'),
      id: 1,
      isOpen: false,
      value: 0
    },
    {
      src: require('../../assets/card3.png'),
      id: 2,
      isOpen: false,
      value: 0
    },
    {
      src: require('../../assets/card4.png'),
       id: 3,
       isOpen: false,
       value: 0
    },
    {
      src: require('../../assets/card5.png'),
      id: 4,
      isOpen: false,
      value: 0
    }, 
    {
      src: require('../../assets/card6.png'), 
      id: 5,
      isOpen: false,
      value: 100
    },
    {
      src: require('../../assets/card7.png'),
       id: 6,
       isOpen: false,
       value: 500
    },
    {
      src: require('../../assets/card8.png'),
       id: 7,
       isOpen: false,
       value: 1000
    },
    {
      src: require('../../assets/card9.png'),
       id: 8,
       isOpen: false,
       value: 0
    },
  ];

function PlayScreen() {
  const [cardsState, setCardsState] = useState(cards);
  const [openCards, setOpenCards] = useState(false)
  const [showCard, setShowCard] = useState (false);
  const [modalVisible, setModalVisible] = useState(false);

  const [click, setClick] = useState(0)
  const [score, setScore] = useState(0);
  const [bestScore, setBestScore] = useState(0)


 function handlOpenCard(){
  
  const newArr = [];
  setCardsState(shuffleArray(cardsState))
  cardsState.forEach((item, index) => {
    newArr[index] = item;
    newArr[index].isOpen = true;
  });

  setCardsState(newArr)
  return newArr
}

function handlCloseCard(){
  const newArr = [];
  cardsState.forEach((item, index) => {
    newArr[index] = item;
    newArr[index].isOpen = false;
  });
  setCardsState(newArr)
  return newArr
}

function handleChooseCard (id) {
  let newClick= click+1
    setClick(newClick)
    console.log('Click ' + click)
  cardsState.forEach((obj)=>{
    if(obj.id==id){
      obj.isOpen=true
      let newScore = score + obj.value
      setScore(newScore)
    }
  })
}

function shuffleArray (arr) {
  let newArr = arr.sort(() => Math.random() - 0.5)
  return newArr;
};



function closeModal(){
  setModalVisible(false)
  setShowCard(false)
  setScore(0)
  handlCloseCard()
}


useEffect(()=>{

  if(modalVisible === false){
  const openTimer =  setTimeout(()=>{
      handlOpenCard()
    }, 2000)
    
   const closeTimer = setTimeout(()=>{
      setShowCard(true)
    }, 5000)
  
 const openCard = setTimeout(()=>{
      handlCloseCard()
    }, 5000)
    
    return () => {
      clearTimeout(openTimer, closeTimer, openCard)
    }
  }
  
}, [ modalVisible])


useEffect(()=>{
  if(click == 3){
  setTimeout(()=>{
      setModalVisible(true)
      setClick(0)
    }, 1000)

   if(score > bestScore){
     setBestScore(score)
   }
  }


},[click])


  return (
     <ImageBackground 
        source={require('../../assets/bg.png')} 
        style={styles.fonImage}
        resizeMode= "cover"
     >
      <ModalGameOver 
        scoreValue={score} 
        bestScoreVal={bestScore}
        handleCloseModal={closeModal} 
        isVisible={modalVisible} 
      />
        <View style={styles.container}>
          <Header scoreValue={score}/>
          <MemoryGame
            cards={cardsState}
            handleOpen={(value)=> setOpenCards(!value)}
            handleCard={handleChooseCard}
          />
          <InfoBlock/>
        </View>
     </ImageBackground>      
  );
};

const styles = StyleSheet.create({
  fonImage: {
    height: "100%",
    width: "100%",
   justifyContent: 'space-between',
   alignItems:'center'

  },
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
   
  },
  header:{
    width: 300,
    height:70,
    resizeMode: 'cover',
    marginBottom: 30,
    marginTop: 20
  },
  headerText: {
    textAlign: 'center',
    fontSize: 23,
    padding: 18,
    color: "#fcff03",
    textTransform: 'uppercase',
    fontWeight: "bold",
    fontFamily: "Cochin"
  },
});

export {
    PlayScreen
} 